#include <Windows.h>
#include <conio.h>
#include <iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;
class node
{
public:
    int x, y;
    node *next;
    node *before;
};

class snake
{
public:
    int i, j, e, m;
    char dir,lastdir;
    node *head, *tail, *t;
    snake()
    {
        i = 0;
        j = 0;
        dir =lastdir= 'd';
        head = tail = 0;
    }
    void adddata(int o, int p)
    {
        node *tmp = new node;
        if (head == 0)
        {
            head = tail = tmp;
            tmp->x = tmp->y = 0;
            head->next = head->before = 0;
            tail->next = tail->before = 0;
        }
        else
        {
            tail->next = tmp;
            tmp->before = tail;
            tmp->next = 0;
            tail = tmp;
            tail->x = o;
            tail->y = p;

        }
    }
    void move(int x) {
        if(lastdir=='d' && dir=='a')
            dir=lastdir;
        if(lastdir=='a' && dir=='d')
            dir=lastdir;
        if(lastdir=='w' && dir=='s')
            dir=lastdir;
        if(lastdir=='s' && dir=='w')
            dir=lastdir;
        if (dir == 'd')
        {
            m = tail->x + 1;
            adddata(m, tail->y);
            t = head;
            head = head->next;
            head->before = 0;
            delete t;
        }
        else if (dir == 'a')
        {
            m = tail->x - 1;
            adddata(m, tail->y);
            t = head;
            head = head->next;
            head->before = 0;
            delete t;
        }
        else if (dir == 's')
        {
            m = tail->y + 1;
            adddata(tail->x, m);
            t = head;
            head = head->next;
            head->before = 0;
            delete t;
        }
        else if (dir == 'w')
        {
            m = tail->y - 1;
            adddata(tail->x, m);
            t = head;
            head = head->next;
            head->before = 0;
            delete t;
        }
            lastdir=dir;


    if (tail->x < 0 || tail->x > x - 1 || tail->y < 0 || tail->y > x - 1)
    {
        system("cls");
        e++;
    }
    for (t = head; t != tail; t = t->next)
    {
        if (tail->x == t->x && tail->y == t->y)
        {
            system("cls");
            e++;
        }
    }
    }
    int food_state_i(int x)
    {
        int o;
        o = rand() % x;
        return o;
    }
    int food_state_j(int x)
    {
        int p;
        p = rand() % x;
        return p;
    }
};
class board:public snake
{

public:
int updated;
int i1, j1, score=0, chap;
void print(int x)
{
    char f = 219;
chap = 0;
cout << "Snake Game "  << endl;
cout << " Your Score    : " << score << endl;
for (int i = 0; i <= x; i++)
    cout << "*";
cout << "*";
cout << endl;
cout << "*";
for (int n = 0; n < x; n++) {
    for (int m = 0; m < x; m++)
    {
        for (t = head; t != tail; t = t->next)
        {
            if (t->x == m && t->y == n)
            {
                cout << f;
                chap++;
            }
        }
        if (chap == 1)
        {
            chap = 0;
            continue;
        }
        if (t->x == m && t->y == n)
        {
            cout << f;
            chap++;
        }
        if (chap >= 1)
        {
            chap = 0;
            continue;
        }
        if (i1 == m && j1 == n)
            cout << f;
        else
            cout << " ";
    }
    cout << "*";
    cout << endl;
    cout << "*";
}
for (int m = 0; m <= x; m++)
    cout << "*";
}
void update(int x)
{
    if (head == 0)
        adddata(1, 1);
    move(x);
    if (tail->x == i1 && tail->y == j1)
    {
        updated++;
        adddata(i1, j1);
        score = score + 10;
        i1 = food_state_i(x);
        j1 = food_state_j(x);
        while (true)
        {
            if (i1 == 0 && j1 == 0)
            {
                i1 = food_state_i(x);
                j1 = food_state_j(x);
            }
            else if (i1 == 0 && j1 == x - 1)
            {
                i1 = food_state_i(x);
                j1 = food_state_j(x);
            }
            else if (i1 == x - 1 && j1 == 0)
            {
                i1 = food_state_i(x);
                j1 = food_state_j(x);
            }
            else if (i1 == x - 1 && j1 == x - 1)
            {
                i1 = food_state_i(x);
                j1 = food_state_j(x);
            }
            else
            {

                break;
            }
        }
    }
}
void run()
{
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   SNAKE GAME      ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   1.Play          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   2.Exit          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    int anser;
    cin >> anser;
    if (anser == 2)
    {
        exit(0);
    }
    if (anser == 1)
    {
        board b;
        int x;
        int i = 100;
        srand(time(0));
        cin >> x;
        b.e = 0;
        b.i1 = b.food_state_i(x);
        b.j1 = b.food_state_j(x);
        while (true)
        {
            if (b.e > 0)
                break;
            if (_kbhit())
                b.dir = _getch();
            b.update(x);
            b.print(x);
            if (b.score % 40 == 0 && b.score != 0 && i != 25 && b.updated == 1)
                i = i - 25;
            b.updated = 0;
            Sleep(i);
            system("cls");
        }
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << "~                   ~" << endl;
        cout << "~     GAME OVER     ~" << endl;
        cout << "~                   ~" << endl;
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << " SCORE : " << b.score << endl;
    }
}
};
