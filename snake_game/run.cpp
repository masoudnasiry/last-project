#include"Snake.h"
#include"board.h"
#include"snake_game.h"
void snake_game::run()
{
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   SNAKE GAME      ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   1.Play          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~   2.Exit          ~" << endl;
    cout << "~                   ~" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
    int anser;
    cin >> anser;
    if (anser == 2)
    {
        exit(0);
    }
    if (anser == 1)
    {
        int ff;
        double i = 128;
        srand(time(0));
        cin >> ff;
        getboard()->wlifeleft(2);
        getboard()->wscore(0);
        getboard()->getsnake()->we(0);
        getboard()->wi1(getboard()->getsnake()->collectible_generator_i(ff));
        getboard()->wj1(getboard()->getsnake()->collectible_generator_j(ff));
        getboard()->wrand1(rand()%6);
        getboard()->wrand2(rand()%4);
        getboard()->wlo(-1);
        getboard()->wlp(-1);
        getboard()->wso(-1);
        getboard()->wsp(-1);
        getboard()->wsuo(-1);
        getboard()->winp(-1);
        getboard()->wpo(-1);
        getboard()->wlp(-1);
        getboard()->wpp(-1);
        getboard()->wlp(-1);
        getboard()->wlifeo(-1);
        getboard()->wlifep(-1);
        getboard()->wi(i);
        getboard()->wendpo(0);
        getboard()->getfood()->savefood(getboard()->geti1(),getboard()->getj1());
        getboard()->wirand(getboard()->geti1());
        getboard()->wjrand(getboard()->getj1());
        while (true)
        {
            if (getboard()->getsnake()->gete() > 0)
            {
                if(getboard()->getlifeleft()==0)
                    break;
                else
                {
                    getboard()->wlifeleft(getboard()->getlifeleft()-1);
                    getboard()->getsnake()->we(0);
                    getboard()->getsnake()->resetsnake();
                    getboard()->wendpo(0);
                    getboard()->getsnake()->wstart(0);
                    i=128;
                    getboard()->getsu()->wend(0);
                    Sleep(1000);
                }
            }
            if (_kbhit())
            {
                getboard()->getsnake()->wdir(_getch());
                getboard()->getsnake()->wsavedir(getboard()->getsnake()->getdir());
            }
            getboard()->update(ff);
            getboard()->print(ff);
            i=getboard()->geti();
            getboard()->wupdated(0);
            Sleep(i);
            system("cls");
        }
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << "~                   ~" << endl;
        cout << "~     GAME OVER     ~" << endl;
        cout << "~                   ~" << endl;
        cout << "~~~~~~~~~~~~~~~~~~~~~" << endl;
        cout << " SCORE : " << getboard()->getscore0() << endl;
    }
}


