QT += core
QT -= gui

TARGET = Snakegame
CONFIG += console
CONFIG -= app_bundle
CONFIG  +=c++11
TEMPLATE = app

SOURCES += main.cpp \
    boardprint.cpp \
    snakeadddata.cpp \
    snakectr.cpp \
    snakemove.cpp \
    foodgeneratori.cpp \
    foodgeneratorj.cpp \
    boardupdate.cpp \
    getsnake.cpp \
    getnode.cpp \
    savefood.cpp \
    getfood.cpp \
    run.cpp \
    getboard.cpp \
    boardoveload.cpp \
    collectiblefunc.cpp \
    getcol.cpp \
    getlr.cpp \
    getscore.cpp \
    getinv.cpp \
    getsu.cpp \
    getpioson.cpp \
    getlife.cpp \
    invertersnake.cpp \
    reducesnake.cpp \
    addlife.cpp \
    poisoning.cpp \
    addingscore.cpp \
    sup.cpp \
    snreduce.cpp \
    resetsnake.cpp \
    invert.cpp
HEADERS += Snake.h \
    board.h \
    node.h \
    food.h \
    snake_game.h \
    collectible.h \
    collectible.h \
    length_reducer.h \
    score.h \
    inverter.h \
    speedup.h \
    poison.h \
    life.h

OTHER_FILES +=

