#ifndef _snake_H_
#define _snake_H_
#include <Windows.h>
#include <conio.h>
#include <iostream>
#include<stdlib.h>
#include<time.h>
#include"node.h"
using namespace std;
class snake
{
    node *head,*tail, *t,*tmp,*tmp1,*tmp2;
    int i, j, e, m,ii,inv,start;
    char savedir,dir,lastdir;
public:
    snake();
    int getsavedir(){return savedir;}
    int gete(){return e;}
    int getdir(){return dir;}
    void we(int xx){e=xx;}
    void wstart(int xx){start=xx;}
    void wdir(int xx){dir=xx;}
    void wsavedir(int xx){savedir=xx;}
    node *gethead(){return head;}
    node *gettail(){return tail;}
    void adddata(int o, int p);
    void move(int x) ;
    int collectible_generator_i(int x);
    int collectible_generator_j(int x);
    int reduce();
    void resetsnake();
    void invert();
};
#endif
