#include"Snake.h"
#include"board.h"
#include"length_reducer.h"
void board::update(int ff)
{
    board a;
    a.add=0;
    if (getsnake()->gethead() == 0)
    {
        getsnake()->adddata(1, 1);
    }
    if(getsnake()->gettail()->getx() == po &&getsnake()->gettail()->gety() == pp)
    {
        endpo=15;
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        po=pp=-1;
    }
    if(rand1==0 && rand2==0)
    {
        po=getpoison()->collectpoo(ff);
        pp=getpoison()->collectpop(ff);
    }
    if(endpo!=0)
    {
        getsnake()->wsavedir(getpoison()->poisoning(getsnake()->getsavedir()));
        endpo--;
    }
    if(getsu()->getend()>0)
        getsu()->wend(getsu()->getend()-1);
    if(getsu()->getend()==0 && i!=128)
        i=i*2;
    if(getsnake()->gettail()->getx() == ino &&getsnake()->gettail()->gety() == inp)
    {
        getinv()->invertsnake();
        getsnake()->invert();
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        ino=inp=-1;
    }
    if(rand1==1 && rand2==0)
    {
        ino=getinv()->collectino(ff);
        inp=getinv()->collectinp(ff);
    }
    if(getsnake()->gettail()->getx() == lo &&getsnake()->gettail()->gety() == lp)
    {
        getlr()->reducesnake();
        dscore=getsnake()->reduce();
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        lo=lp=-1;
    }
    if(rand1==2 && rand2==0)
    {
        lo=getlr()->collectio(ff);
        lp=getlr()->collectip(ff);
    }
    if(getsnake()->gettail()->getx() == lifep &&getsnake()->gettail()->gety() == lifeo)
    {
        lifeleft=getlife()->addlife(lifeleft);
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        lifeo=lifep=-1;
    }
    if(rand1==3 && rand2==0)
    {
        lifeo=getscore()->collectso(ff);
        lifep=getscore()->collectsp(ff);
    }
    if(getsnake()->gettail()->getx() == so &&getsnake()->gettail()->gety() == sp)
    {
        score=getscore()->addingscore(score);
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        so=sp=-1;
    }
    if(rand1==4 && rand2==0)
    {
        so=getscore()->collectso(ff);
        sp=getscore()->collectsp(ff);
    }
    if(getsnake()->gettail()->getx() == suo &&getsnake()->gettail()->gety() == sup)
    {
        i=getsu()->sup(i);
        rand1=getsnake()->collectible_generator_i(6);
        rand2=getsnake()->collectible_generator_i(4)+1;
        suo=sup=-1;
    }
    if(rand1==5 && rand2==0)
    {
        suo=getscore()->collectsuo(ff);
        sup=getscore()->collectsup(ff);
    }
    if(rand2>=0)
        rand2--;
    if(lifeo==-1 && lifep==-1 && lo==-1 && lp==-1 && po==-1 && pp==-1)
        if(suo==-1 && sup==-1 && so==-1 && sp==-1 && ino==-1 && inp==-1)
            rand2=0;
    if (getsnake()->gettail()->getx() == i1 &&getsnake()->gettail()->gety() == j1)
    {
        updated++;
        a++;
        if(a.add==1)
            getsnake()->adddata(i1, j1);
        score = score + 10;
        i1 =getsnake()->collectible_generator_i(ff);
        j1 = getsnake()->collectible_generator_j(ff);
        while (true)
        {
            if (i1 == 0 && j1 == 0)
            {
                i1 = getsnake()->collectible_generator_i(ff);
                j1 = getsnake()->collectible_generator_j(ff);
            }
            else if (i1 == 0 && j1 == ff - 1)
            {
                i1 = getsnake()->collectible_generator_i(ff);
                j1 = getsnake()->collectible_generator_j(ff);
            }
            else if (i1 == ff - 1 && j1 == 0)
            {
                i1 = getsnake()->collectible_generator_i(ff);
                j1 = getsnake()->collectible_generator_j(ff);
            }
            else if (i1 == ff - 1 && j1 == ff - 1)
            {
                i1 = getsnake()->collectible_generator_i(ff);
                j1 = getsnake()->collectible_generator_j(ff);
            }
            else
            {
                getfood()->savefood(i1,j1);
                irand=i1;
                jrand=j1;
                break;
            }
        }
    }
    getsnake()->move(ff);
}
