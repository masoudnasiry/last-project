#ifndef _board_H_
#define _board_H_
#include"Snake.h"
#include"food.h"
#include"collectible.h"
#include "length_reducer.h"
#include"score.h"
#include"inverter.h"
#include"speedup.h"
#include "poison.h"
#include"life.h"
class board
{
    int rand1,rand2;
    int add,lo,lp,so,sp,ino,inp,suo,sup,po,pp,lifeo,lifep,dscore,endpo,lifeleft;
    double i;
    int updated;
    int i1, j1,irand,jrand;
    int score, chap;
    snake s;
    node s1;
    food s2;
    collectible s3;
    length_reducer s4;
    addscore s5;
    inverter s6;
    speedup s7;
    poison s8;
    life s9;
    node *t;
public:
    int geti1(){return i1;}
    int getj1(){return j1;}
    int getlifeleft(){return lifeleft;}
    int geti(){return i;}
    int getscore0(){return score;}
    void wlifeleft(int xx){lifeleft=xx;}
    void wscore(int xx){score=xx;}
    void wi1(int xx){i1=xx;}
    void wj1(int xx){j1=xx;}
    void wendpo(int xx){endpo=xx;}
    void wirand(int xx){irand=xx;}
    void wjrand(int xx){jrand=xx;}
    void wupdated(int xx){updated=xx;}
    void wi(int xx){i=xx;}
    void wrand1(int xx){rand1=xx;}
    void wrand2(int xx){rand2=xx;}
    void wpo(int xx){po=xx;}
    void wpp(int xx){pp=xx;}
    void wlo(int xx){lo=xx;}
    void wlp(int xx){lp=xx;}
    void wso(int xx){so=xx;}
    void wsp(int xx){sp=xx;}
    void wino(int xx){ino=xx;}
    void winp(int xx){inp=xx;}
    void wsuo(int xx){suo=xx;}
    void wsup(int xx){sup=xx;}
    void wlifeo(int xx){lifeo=xx;}
    void wlifep(int xx){lifep=xx;}
    collectible *getcol();
    length_reducer *getlr();
    addscore *getscore();
    inverter *getinv();
    speedup *getsu();
    poison *getpoison();
    life *getlife();
    snake *getsnake();
    node *getnode();
    food *getfood();
    void print(int x);
    void update(int x);
    void run();
    board operator ++(int);
};
#endif
